package com.cmg.profileevaluation

import android.app.Application
import com.cmg.profileevaluation.network.MyApi
import com.cmg.profileevaluation.repositary.ArticleListRepositary
import com.cmg.profileevaluation.viewmodel.viewmodelfactory.ArticlesViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class AppController: Application(),KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@AppController))
        bind() from singleton { MyApi(instance()) }

        bind() from singleton { ArticleListRepositary(instance()) }
        bind() from provider { ArticlesViewModelFactory(instance()) }
    }
}