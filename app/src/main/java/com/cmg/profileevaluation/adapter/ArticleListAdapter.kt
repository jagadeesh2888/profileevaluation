package com.cmg.profileevaluation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cmg.profileevaluation.R
import com.cmg.profileevaluation.data.ArticleListData
import com.cmg.profileevaluation.databinding.ArticleListItemBinding

class ArticleListAdapter(
    private val articleList:List<ArticleListData>
):RecyclerView.Adapter<ArticleListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.article_list_item,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.articleListItemBinding.articles = articleList[position]
    }

    override fun getItemCount() = articleList.size

    inner class MyViewHolder(
        val articleListItemBinding: ArticleListItemBinding
    ):RecyclerView.ViewHolder(articleListItemBinding.root)
}