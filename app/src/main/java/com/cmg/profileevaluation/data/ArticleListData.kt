package com.cmg.profileevaluation.data

data class ArticleListData(
    val title:String?,
    val date_publish:String?
)
