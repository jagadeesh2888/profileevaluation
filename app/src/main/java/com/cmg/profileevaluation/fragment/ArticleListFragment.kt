package com.cmg.profileevaluation.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cmg.profileevaluation.R
import com.cmg.profileevaluation.adapter.ArticleListAdapter
import com.cmg.profileevaluation.databinding.FragmentArticleListBinding
import com.cmg.profileevaluation.listener.ResponseListener
import com.cmg.profileevaluation.util.*
import com.cmg.profileevaluation.viewmodel.ArticlesViewModel
import com.cmg.profileevaluation.viewmodel.viewmodelfactory.ArticlesViewModelFactory
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ArticleListFragment : Fragment(R.layout.fragment_article_list),KodeinAware,ResponseListener {

    override val kodein by kodein()
    private val factory: ArticlesViewModelFactory by instance()

    private lateinit var articlesBinding:FragmentArticleListBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        articlesBinding = FragmentArticleListBinding.bind(view)
        activity.let {
            it?.hideKeyBoard()
        }
        val articleViewModel = ViewModelProvider(this,factory).get(ArticlesViewModel::class.java)
        articleViewModel.responseListener = this
        articlesBinding.articleviewmodel = articleViewModel

        when(val navigateFrom = arguments?.getString(Passparams.NAVIGATE_FROM,"")){
            Passparams.SEARCH -> articleViewModel.getArticles(arguments?.getString(Passparams.SEARCH_TXT,"")!!)
            else -> articleViewModel.getMostPopularAPI(navigateFrom!!)
        }



        articleViewModel.articleList.observe(viewLifecycleOwner, Observer {list->
            articlesBinding.recyclerview.also {recyclerView->
                val itemDecorder = RecyclerviewItemDecoration(view.context)
                recyclerView.addItemDecoration(itemDecorder)
                recyclerView.adapter = ArticleListAdapter(list)
            }
        })
    }


    override fun onStarted() {
        show(articlesBinding.progressBar)
    }

    override fun onSuccess(msg: String) {
        hide(articlesBinding.progressBar)
        context?.toast(msg)
    }

    override fun onError(msg: String) {
        hide(articlesBinding.progressBar)
        context?.toast(msg)
    }
}