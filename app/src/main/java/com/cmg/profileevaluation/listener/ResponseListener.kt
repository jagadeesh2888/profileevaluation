package com.cmg.profileevaluation.listener

interface ResponseListener {
    fun onStarted()
    fun onSuccess(msg:String)
    fun onError(msg: String)
}