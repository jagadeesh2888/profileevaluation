package com.cmg.profileevaluation.model.mostPopular

data class MediaMetadata(
    val format: String,
    val height: Int,
    val url: String,
    val width: Int
)