package com.cmg.profileevaluation.model.searchResponse

data class ArticleResponse(
    val copyright: String,
    val response: Response,
    val status: String
)