package com.cmg.profileevaluation.model.searchResponse

data class Meta(
    val hits: Int,
    val offset: Int,
    val time: Int
)