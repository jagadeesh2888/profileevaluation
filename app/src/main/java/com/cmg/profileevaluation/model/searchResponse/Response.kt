package com.cmg.profileevaluation.model.searchResponse

data class Response(
    val docs: List<Doc>,
    val meta: Meta
)