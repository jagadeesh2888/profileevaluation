package com.cmg.profileevaluation.network

import android.content.Context
import com.cmg.profileevaluation.model.mostPopular.MostPopularResponse
import com.cmg.profileevaluation.model.searchResponse.ArticleResponse
import com.cmg.profileevaluation.util.Passparams
import com.cmg.profileevaluation.util.RetrofitClientInstance
import retrofit2.Response
import retrofit2.http.*

interface MyApi {

    @GET(Passparams.SEARCH_ARTICLES_API)
    suspend fun getArticles(@Query("q") searchTxt:String,@Query("api-key") myKey:String):Response<ArticleResponse>

    @GET(Passparams.MOST_VIEWED_ARTICLES_API)
    suspend fun getMostViewedArticles(@Query("api-key") myKey:String):Response<MostPopularResponse>

    @GET(Passparams.MOST_EMAILED_ARTICLES_API)
    suspend fun getMostEmailArticles(@Query("api-key") myKey:String):Response<MostPopularResponse>

    @GET(Passparams.MOST_SHARED_ARTICLES_API)
    suspend fun getMostSharedArticles(@Query("api-key") myKey:String):Response<MostPopularResponse>



    companion object{
        operator fun invoke(
            context: Context
        ) : MyApi{
            return RetrofitClientInstance(context).create(MyApi::class.java)
        }
    }
}