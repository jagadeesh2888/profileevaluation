package com.cmg.profileevaluation.repositary


import com.cmg.profileevaluation.model.mostPopular.MostPopularResponse
import com.cmg.profileevaluation.model.searchResponse.ArticleResponse
import com.cmg.profileevaluation.network.MyApi
import com.cmg.profileevaluation.util.Passparams
import com.cmg.profileevaluation.util.SafeAPIRequest

class ArticleListRepositary(
    private val myApi: MyApi,
):SafeAPIRequest() {

    suspend fun getArticles(searchTxt:String): ArticleResponse {
        return apiRequest {
            myApi.getArticles(searchTxt,Passparams.MYKEY)
        }
    }

    suspend fun getMostViewed():MostPopularResponse{
        return apiRequest {
            myApi.getMostViewedArticles(Passparams.MYKEY)
        }
    }

    suspend fun getMostEmailed():MostPopularResponse{
        return apiRequest {
            myApi.getMostEmailArticles(Passparams.MYKEY)
        }
    }

    suspend fun getMostShared():MostPopularResponse{
        return apiRequest {
            myApi.getMostSharedArticles(Passparams.MYKEY)
        }
    }

}