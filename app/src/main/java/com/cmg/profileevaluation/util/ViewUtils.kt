package com.cmg.profileevaluation.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.Toast
import java.lang.NullPointerException
import java.text.ParseException
import java.text.SimpleDateFormat

fun Context.toast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun show(progressBar: ProgressBar){
    if (progressBar.visibility == View.GONE)
        progressBar.visibility = View.VISIBLE
}

fun hide(progressBar: ProgressBar){
    if (progressBar.visibility == View.VISIBLE)
        progressBar.visibility = View.GONE
}

fun changeDateFormatFromISO8601(dateString: String):String?{
    val currentDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ")
    val resultDateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
    try {
        val date = currentDateFormat.parse(dateString)
        return resultDateFormat.format(date)
    }catch (e: ParseException){
        e.printStackTrace()
    }catch (e:NullPointerException){
        e.printStackTrace()
    }
    return dateString
}

fun changeDateFormat(dateString: String):String?{
    val currentDateFormat = SimpleDateFormat("yyyy-MM-dd")
    val resultDateFormat = SimpleDateFormat("dd/MM/yyyy")
    try {
        val date = currentDateFormat.parse(dateString)
        return resultDateFormat.format(date)
    }catch (e: ParseException){
        e.printStackTrace()
    }catch (e:NullPointerException){
        e.printStackTrace()
    }
    return dateString
}

fun Activity.hideKeyBoard(){
    val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
}