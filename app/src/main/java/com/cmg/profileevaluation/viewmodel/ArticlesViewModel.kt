package com.cmg.profileevaluation.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cmg.profileevaluation.data.ArticleListData
import com.cmg.profileevaluation.listener.ResponseListener
import com.cmg.profileevaluation.model.mostPopular.MostPopularResponse
import com.cmg.profileevaluation.repositary.ArticleListRepositary
import com.cmg.profileevaluation.util.*
import org.json.JSONException
import java.lang.Exception

class ArticlesViewModel(
    private val articlesRepositary: ArticleListRepositary
):ViewModel() {

    var responseListener:ResponseListener?=null

    val _articleList:MutableLiveData<List<ArticleListData>> = MutableLiveData()
    val articleList:LiveData<List<ArticleListData>>
        get() = _articleList

    val errorMsg = ObservableField<String>()

    fun getArticles(searchTxt:String) {
        val list = ArrayList<ArticleListData>()
        Couritnes.main {
            try {
                val response = articlesRepositary.getArticles(searchTxt)
                if (response.status == "OK"){
                    responseListener?.onSuccess("success")
                    if (response.response.docs.isNotEmpty()){
                        response.response.docs.forEach {
                            val articleListData = ArticleListData(
                                it.headline.print_headline,
                                changeDateFormatFromISO8601(it.pub_date),
                            )
                            list.add(articleListData)

                        }

                        _articleList.value = list
                    }
                }
            }catch (apiException:APIException){
                errorMsg.set(apiException.message!!)
                responseListener?.onError(apiException.message!!)
            }catch (internet:NoInternetException){
                errorMsg.set(internet.message!!)
                responseListener?.onError(internet.message!!)
            }catch (jsonException:JSONException){
                errorMsg.set(jsonException.message!!)
                responseListener?.onError(jsonException.message!!)
            }catch (exception:Exception){
                errorMsg.set(exception.message!!)
                responseListener?.onError(exception.message!!)
            }
        }
    }

    fun getMostPopularAPI(keyWord:String){
        val list = ArrayList<ArticleListData>()
        Couritnes.main {
            try {
                var response:MostPopularResponse?=null
                when(keyWord){
                    Passparams.MOST_VIEWED -> response = articlesRepositary.getMostViewed()
                    Passparams.MOST_SHARED -> response = articlesRepositary.getMostShared()
                    Passparams.MOST_EMAILED -> response = articlesRepositary.getMostEmailed()
                    else -> responseListener?.onError("")
                }
                if (response != null) {
                    if (response.status == "OK") {
                        if (response.results.isNotEmpty()){
                            response.results.forEach {
                                val articleListData = ArticleListData(
                                    it.title,
                                    changeDateFormat(it.published_date),
                                )
                                list.add(articleListData)
                            }

                            _articleList.value = list
                            responseListener?.onSuccess("success")
                        }
                    }
                }
            }catch (apiException:APIException){
                errorMsg.set(apiException.message!!)
                responseListener?.onError(apiException.message!!)
            }catch (internet:NoInternetException){
                errorMsg.set(internet.message!!)
                responseListener?.onError(internet.message!!)
            }catch (jsonException:JSONException){
                errorMsg.set(jsonException.message!!)
                responseListener?.onError(jsonException.message!!)
            }catch (exception:Exception){
                errorMsg.set(exception.message!!)
                responseListener?.onError(exception.message!!)
            }
        }
    }

}