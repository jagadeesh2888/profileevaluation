package com.cmg.profileevaluation.viewmodel.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cmg.profileevaluation.repositary.ArticleListRepositary
import com.cmg.profileevaluation.viewmodel.ArticlesViewModel

class ArticlesViewModelFactory(
    private val articlesRepositary:ArticleListRepositary
):ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ArticlesViewModel(articlesRepositary) as T
    }
}